resource "null_resource" "MultiHelloGitLab" {
  count = "${var.multiple}"
  provisioner "local-exec" {
    command = "echo Hello GitLab ${count.index + 1}!"
  }
}
